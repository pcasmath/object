// Element defines an abstract element of a parent.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package object

import (
	"fmt"
	"reflect"
)

// Hasher is the interface satisfied by a Hash method,
type Hasher interface {
	// Hash returns the hash value of the element.
	Hash() uint32
}

// Element is the interface satisfied by an object that belongs to a Parent.
type Element interface {
	fmt.Stringer
	Hasher
	// Parent returns the parent of the element.
	Parent() Parent
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// CombineHash returns a new hash value given by combining h1 and h2.
func CombineHash(h1 uint32, h2 uint32) uint32 {
	return h1 ^ (h2 + 0x9e3779b9 + (h1 << 6) + (h1 >> 2))
}

// SliceToElementSlice copies the entries of the given slice S, all of which are assumed to satisfy the Element interface, into a new slice of Elements. The new slice will have the same capacity as S. This will return an error if S is not a slice, or if the entries of S do not satisfy the Element interface.
func SliceToElementSlice(S interface{}) ([]Element, error) {
	// S has better be a slice
	v := reflect.ValueOf(S)
	if v.Kind() != reflect.Slice {
		return nil, ErrArgNotASliceOfElements
	}
	// Create the destination slice
	T := make([]Element, 0, v.Cap())
	// Copy over the elements
	n := v.Len()
	for i := 0; i < n; i++ {
		x, ok := v.Index(i).Interface().(Element)
		if !ok {
			return nil, ErrArgNotASliceOfElements
		}
		T = append(T, x)
	}
	return T, nil
}
