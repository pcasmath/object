// Tests element.go.

/*
To the extent possible under law, the author(s) have dedicated all copyright and
related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with
this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
*/

package object

import (
	"errors"
	"strconv"
	"testing"
)

// dummyParent is a dummy parent used for testing.
type dummyParent struct{}

// dummyElement is a dummy element used for testing.
type dummyElement int

/////////////////////////////////////////////////////////////////////////
// dummyParent functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the parent.
func (dummyParent) String() string {
	return "zZ(int)"
}

// Contains returns true iff x is an element of this parent, or can naturally be regarded as an element of this parent.
func (dummyParent) Contains(x Element) bool {
	_, ok := x.(dummyElement)
	return ok
}

// ToElement returns x as an element of this parent, or an error if x cannot naturally be regarded as an element of this parent.
func (dummyParent) ToElement(x Element) (Element, error) {
	y, ok := x.(dummyElement)
	if !ok {
		return nil, errors.New("argument is not a dummyElement")
	}
	return y, nil
}

// AreEqual returns true iff x and y are both contained in the parent, and x = y.
func (dummyParent) AreEqual(x Element, y Element) (bool, error) {
	xx, ok := x.(dummyElement)
	if !ok {
		return false, errors.New("argument 1 is not a dummyElement")
	}
	yy, ok := y.(dummyElement)
	if !ok {
		return false, errors.New("argument 2 is not a dummyElement")
	}
	return xx == yy, nil
}

/////////////////////////////////////////////////////////////////////////
// dummyElement functions
/////////////////////////////////////////////////////////////////////////

// String returns a string representation of the element.
func (x dummyElement) String() string {
	return strconv.Itoa(int(x))
}

// Hash returns a hash value for the element.
func (x dummyElement) Hash() uint32 {
	return uint32(x)
}

// Parent returns the parent of the element.
func (dummyElement) Parent() Parent {
	return dummyParent{}
}

/////////////////////////////////////////////////////////////////////////
// Test functions
/////////////////////////////////////////////////////////////////////////

// TestSliceToElementSlice tests SliceToElementSlice.
func TestSliceToElementSlice(t *testing.T) {
	// Create a slice of dummy objects
	S := make([]dummyElement, 0, 10)
	for i := 0; i < cap(S); i++ {
		S = append(S, dummyElement(i))
	}
	// Convert the slice to a slice of object.Elements
	P := dummyParent{}
	T, err := SliceToElementSlice(S)
	if err != nil {
		t.Fatalf("unexpected error calling SliceToElementSlice: %v", err)
	} else if len(T) != len(S) {
		t.Fatalf("slice returned by SliceToElementSlice is of incorrect length: expected %d but got %d", len(S), len(T))
	}
	// Check that the values agree
	for i, x := range S {
		ok, err := P.AreEqual(x, T[i])
		if err != nil {
			t.Fatalf("unexpected error calling AreEqual (index: %d): %v", i, err)
		} else if !ok {
			t.Fatalf("expected AreEqual to return true but got false (index: %d)", i)
		}
	}
	// Now create a slice of non-object.Elements
	U := make([]int, 0, 10)
	for i := 0; i < cap(U); i++ {
		U = append(U, i)
	}
	// Attempting to convert bogus data should error
	if _, err = SliceToElementSlice(U); err == nil {
		t.Fatalf("unexpected error calling SliceToElementSlice: %v", err)
	} else if _, err := SliceToElementSlice(dummyElement(1)); err == nil {
		t.Fatal("expected error calling SliceToElementSlice but got nil")
	} else if _, err = SliceToElementSlice(int(1)); err == nil {
		t.Fatal("expected error calling SliceToElementSlice but got nil")
	}
}

// TestCombineHash tests the CombineHash function.
func TestCombineHash(t *testing.T) {
	for _, I := range [][]uint32{
		{0, 1, 2654435770},
		{1, 0, 2654435832},
		{0, 0, 2654435769},
		{1, 1, 2654435835},
		{1, 5, 2654435839},
		{5, 1, 2654436094},
		{2, 10, 2654435905},
		{100, 101, 2654442323},
	} {
		h1, h2 := I[0], I[1]
		h := CombineHash(h1, h2)
		if h != I[2] {
			t.Fatalf("combined hash of %d and %d gives %d but expected %d", h1, h2, h, I[2])
		}
	}
}
